module Config where

import Prelude
import Network.Eth.FriendInDebt (UserAddress(..))

friendInDebtAddress = (UserAddress "0x63d633de3fa2fbd6a7ada6f7af6317d897e29269")

checkInterval = 7000
