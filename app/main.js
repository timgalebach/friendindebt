var FriendInDebt;
var fid;
var acct1 = "0x406Dd5315e6B63d6F1bAd0C4ab9Cd8EBA6Bb1bD2";
var jared = "0xFa8ca29Acd1a27982F5a5669192A7048C0B990c8";
var contractAddress = "0x63d633de3fa2fbd6a7ada6f7af6317d897e29269";

var state = {"friends": [],
             "pending": {},
             "balances": {}};

var validateAddress = function(address) {
    if(address.substring(0,2) != "0x") {
        return "Address must start with '0x'";
    }
    else if(address.length != 42) {
        return "Address must have 40 characters after the '0x'";
    }
    return "";
};

function initWeb3() {
    if (typeof web3 !== 'undefined') {
        window.web3 = new Web3(web3.currentProvider);
//        console.log(web3.currentProvider);
    } else {
        console.log('No web3? You should consider trying MetaMask!')
        window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
    }
}

var init = function() {
    FriendInDebt = TruffleContract(friendInDebtConfig);
    FriendInDebt.setProvider(web3.currentProvider);
    window.setTimeout(refreshFriends("#friendList", "#pendingList"), 2000);
};

var checkFriends = function() {
    FriendInDebt.at(contractAddress).then(function(instance) {
        fid = instance;
        return fid.getFriends.call(web3.eth.accounts[0]);
    }).then(function(res) {
        console.log(res.valueOf());
    });
};

var addFriend = function(address) {
    FriendInDebt.at(contractAddress).then(function(instance) {
        fid = instance;
        return fid.createFriendship(address);
    }).then(function(tx) {
        console.log("Friend: " + address + " added.");
    });
};

var typeAddress = function() {
    var address = document.getElementById("newFriend").value;
    var errorMsg = validateAddress(address);
    if (errorMsg != "" && address.length > 0) {
        document.getElementById("addressError").innerHTML = errorMsg;
        document.getElementById("addFriendBtn").disabled = true;
    }
    else if (errorMsg != "" && address.length == 0) {
        document.getElementById("addressError").innerHTML = "";
    }
    else {
        document.getElementById("addressError").innerHTML = "";
        document.getElementById("addFriendBtn").disabled = false;
    }
};

var newFriend = function() {
    var address = document.getElementById("newFriend").value;
    document.getElementById("newFriend").value = "";
    addFriend(address);
};

var refreshBalance = function(friend) {
    FriendInDebt.at(contractAddress).then(function(instance) {
        fid = instance;
        return fid.getBalance.call(web3.eth.accounts[0], friend);
    }).then(function(res) {
        state.balances[friend] = res.toNumber();
        displayBalances("#balanceList");
    });
};

var refreshPending = function(friendAddress) {
    FriendInDebt.at(contractAddress).then(function(instance) {
        fid = instance;
        f1 = web3.eth.accounts[0];
        f2 = friendAddress;
        return fid.getPendingAmount.call(f1, f2);
    }).then(function(res) {
        state.pending[friendAddress] = res.toNumber();
        displayPending("#pendingList");
    });
};

var refreshAll = function() {
    for(var i=0; i<state.friends.length; i++) {
        refreshPending(state.friends[i]);
        refreshBalance(state.friends[i]);
    }
};

var pendingButtons = function(friend) {
    if (state.pending[friend] == 0) return $("<span></span>");
    var buttons = [];
    buttons.push($("<button class='btn-danger' onclick='cancelPending(\""
                   + friend + "\");'>Cancel</button>"));
    buttons.push($("<button class='btn-success' onclick='confirmPending(\""
                   + friend + "\", " + state.pending[friend] + ");'>Confirm</button>"));
    return buttons;
};

var displayBalances = function(eltSelector) {
    $(eltSelector).empty();

    for(var i=0; i<state.friends.length; i++) {
        var amount = state.balances[state.friends[i]];
        var li = $("<li><strong>" + amount + "</strong> <em>" + state.friends[i].substring(0,8) + "</em></li>");
        if (amount > 0)
            li.attr("class", "negative");
        else if (amount < 0)
            li.attr("class", "positive");
        else
            li.attr("class", "zero");
        $(eltSelector).append(li);
    }
};

var displayPending = function(eltSelector) {
    $(eltSelector).empty();
    if (state.friends.length == 0) {
        $(eltSelector).append($("No pending debts"));
    }
    for(var i=0; i<state.friends.length; i++) {
        if(state.pending[state.friends[i]] != 0) {
            var li = $("<li><strong>" + state.pending[state.friends[i]] + "</strong> <em>" + state.friends[i] + "</em></li>");
            var btns = pendingButtons(state.friends[i]);
            li = li.append(btns[0]);
            li = li.append(btns[1]);
            $(eltSelector).append(li);
        }
    }
};

var checkDebtAmount = function(eltId, btnId) {
    var amount = document.getElementById(eltId).value;
    document.getElementById(btnId).disabled = (amount == 0);
    if (amount == 0) {
        document.getElementById(btnId).className = "btn-warning";
    }
    else if (amount > 0) {
        document.getElementById(btnId).className = "btn-success";
    }
    else {
        document.getElementById(btnId).className = "btn-danger";
    }
};

var sendPending = function(friend, eltId) {
    var amount = document.getElementById(eltId).value;
    FriendInDebt.at(contractAddress).then(function(instance) {
        fid = instance;
        return fid.newPending(friend, amount);
    }).then(function(tx) {
        alert("Your friend " + friend.substring(0, 8) + " has been asked to confirm that he owes you " + amount + ". Your friend is not a girl.");
    });
};

var confirmPending = function(friend, amount) {
    FriendInDebt.at(contractAddress).then(function(instance) {
        fid = instance;
        return fid.confirmPending(friend, amount);
    }).then(function(tx) {
        alert("You have confirmed a debt to " + friend.substring(0, 8) + " for " + amount + ".");
    });
};

var debtBtn = function(friend) {
    var id = "newDebt-" + friend;
    var btnId = id + "-btn";
    return $("<input oninput='checkDebtAmount(\"" + id + "\", \"" + btnId + "\");' placeholder='Amount you owe' id='" + id + "' type='number' value='0' /><button disabled value='0' id='" + btnId + "' class='btn-warning' onclick='sendPending(\"" + friend + "\", \"" + id + "\");'>" + friend.substring(0, 8) + " owes me this much</button>");
};

var refreshFriends = function(eltSelector, pendingSelector) {
    FriendInDebt.at(contractAddress).then(function(instance) {
        fid = instance;
        console.log(web3.eth.accounts[0]);
        return fid.getFriends.call(web3.eth.accounts[0]);
    }).then(function(res) {
        $(eltSelector).empty();
        friends = res.valueOf();
        state.friends = friends;
        for(var i=0; i<friends.length; i++) {
            var li = $("<li></li>");
            if(state.pending[friends[i]] == undefined)
                state.pending[friends[i]] = 0;
            li = li.append(debtBtn(friends[i]));
            $(eltSelector).append(li);
        }
        displayPending(pendingSelector);
        refreshAll();
    });
};
